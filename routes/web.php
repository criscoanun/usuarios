<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dados;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('principal');
});

Route::get('/principal', function () {
    return view('principal');
})->name('principal');

Route::get('/pagina2', [Dados::class,'Dados'])->name('pagina2');
