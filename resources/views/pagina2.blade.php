@extends('template')
@section('conteudo')

    <style>
        h5{
            text-align: left;
        }
        button{
            margin: 10px;
            color: blue;
        }
    </style>
    <h5> Cadastro de Usuários<h5>
    <form>
       <div class="col-sm-4">
                Usuário: <div  id="usuario2" w3-include-html=usuario></div>
        </div>
        <div class="col-sm-4">
                Email: <div type="text" id="email2" w3-include-html=email></div>
        </div>
        <div class="col-sm-4">
                Senha: <div id="senha2" w3-include-html=senha></div>
        </div>
        <button type="button" onclick="window.location='{{ URL::route('principal'); }}'">
            Voltar
        </button>

    </form>
@stop

@section('js')
<script>
        document.getElementById("usuario2").innerHTML = usuario;
        document.getElementById("email2").innerHTML = email;
        document.getElementById("senha2").innerHTML = senha;
</script>
@stop
